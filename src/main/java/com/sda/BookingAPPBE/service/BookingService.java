package com.sda.BookingAPPBE.service;

import com.sda.BookingAPPBE.dto.BookingDto;
import com.sda.BookingAPPBE.mapper.BookingMapper;
import com.sda.BookingAPPBE.model.Booking;
import com.sda.BookingAPPBE.model.Room;
import com.sda.BookingAPPBE.model.RoomStatus;
import com.sda.BookingAPPBE.model.User;
import com.sda.BookingAPPBE.repository.BookingRepository;
import com.sda.BookingAPPBE.repository.RoomRepository;
import com.sda.BookingAPPBE.repository.UserRepository;
import com.sda.BookingAPPBE.validator.BookingValidator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;

@Service
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class BookingService {

    private BookingValidator bookingValidator;
    private UserRepository userRepository;
    private RoomRepository roomRepository;
    private BookingRepository bookingRepository;


    public ResponseEntity addBooking(BookingDto bookingDto) {
        try {
            bookingValidator.validateBooking(bookingDto.getRoomId());
            User user = userRepository.findById(bookingDto.getUserId()).orElseThrow();
            Room room = roomRepository.findById(bookingDto.getRoomId()).orElseThrow();
            double totalPrice = calculateTotalPrice(bookingDto, room.getPrice());
            Booking booking = BookingMapper.toEntity(bookingDto, room, user, totalPrice);
            room.setStatus(RoomStatus.RESERVED);
            roomRepository.save(room);
            bookingRepository.save(booking);
            return ResponseEntity.ok(bookingDto);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    private double calculateTotalPrice(BookingDto bookingDto, double pricePerDay) {
        long range = ChronoUnit.DAYS.between(bookingDto.getCheckInDate(),
                bookingDto.getCheckOutDate());
        return range * pricePerDay;
    }
}
