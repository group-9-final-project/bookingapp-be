package com.sda.BookingAPPBE.service;

import com.sda.BookingAPPBE.dto.RoomDto;
import com.sda.BookingAPPBE.exception.ResourceNotFoundException;
import com.sda.BookingAPPBE.mapper.RoomMapper;
import com.sda.BookingAPPBE.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class RoomService {

    private RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public RoomDto getRoomDescription(BigInteger id) {
        return roomRepository.findById(id)
                .map(RoomMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Room with " +
                        "id " + id + " does not exist!"));
    }
}
