package com.sda.BookingAPPBE.service;

import com.sda.BookingAPPBE.dto.LoginRequestDto;
import com.sda.BookingAPPBE.dto.UserDto;
import com.sda.BookingAPPBE.mapper.UserMapper;
import com.sda.BookingAPPBE.model.Address;
import com.sda.BookingAPPBE.model.User;
import com.sda.BookingAPPBE.repository.UserRepository;
import com.sda.BookingAPPBE.validator.LoginUserValidator;
import com.sda.BookingAPPBE.validator.RegisterUserValidator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class UserService {

    private UserRepository userRepository;
    private RegisterUserValidator registerUserValidator;
    private LoginUserValidator loginUserValidator;

    public ResponseEntity registerUser(UserDto userDto) {
        try {
            User user = registerUserValidator.checkIfPasswordAndConfirmPasswordMatch(userDto);
            Address address = userDto.getAddress();
            registerUserValidator.checkIfUserDtoFieldsAndAddressFieldsAreEmpty(userDto, address);
            registerUserValidator.validateUserEmailAddress(address);
            user.setAddress(address);
            userRepository.save(user);
            return ResponseEntity.ok(userDto);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity loginUser(LoginRequestDto loginRequestDto) {
        try {
            User user = loginUserValidator.validateUserLogin(loginRequestDto);
            return ResponseEntity.ok(UserMapper.toDto(user));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}

