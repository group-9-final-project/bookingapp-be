package com.sda.BookingAPPBE.model;

public enum RoomStatus {
    RESERVED,
    FREE
}
