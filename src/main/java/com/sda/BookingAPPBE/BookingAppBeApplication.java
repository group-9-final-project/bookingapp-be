package com.sda.BookingAPPBE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingAppBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookingAppBeApplication.class, args);
	}
}
