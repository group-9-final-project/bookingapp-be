package com.sda.BookingAPPBE.mapper;

import com.sda.BookingAPPBE.dto.BookingDto;
import com.sda.BookingAPPBE.model.Booking;
import com.sda.BookingAPPBE.model.Room;
import com.sda.BookingAPPBE.model.User;

public class BookingMapper {

    public static BookingDto toDto(Booking booking) {
        return BookingDto.builder()
                .roomId(booking.getRoom().getId())
                .userId(booking.getUser().getId())
                .checkInDate(booking.getCheckIn())
                .checkOutDate(booking.getCheckOut())
                .build();
    }

    public static Booking toEntity(BookingDto bookingDto, Room room,
                                   User user, double totalPrice) {
        return Booking.builder()
                .checkIn(bookingDto.getCheckInDate())
                .checkOut(bookingDto.getCheckOutDate())
                .room(room)
                .user(user)
                .totalPrice(totalPrice)
                .build();
    }
}
