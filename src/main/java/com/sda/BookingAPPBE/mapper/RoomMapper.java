package com.sda.BookingAPPBE.mapper;

import com.sda.BookingAPPBE.dto.RoomDto;
import com.sda.BookingAPPBE.model.Room;

public class RoomMapper {

    public static RoomDto toDto(Room room) {
        return RoomDto.builder()
                .id(room.getId())
                .type(room.getType())
                .status(room.getStatus())
                .description(room.getDescription())
                .price(room.getPrice())
                .build();
    }
}
